import random


WORDLIST = 'reci.txt'
SCOREFILE = 'hall_of_fame.txt'


def readFile(sourceFile):
    with open(sourceFile, 'r') as r_file:
        content = r_file.read()
        words = content.split()
    return words


def writeFile(scoreFile, username, secret_word, attempts):
    with open(scoreFile, 'a+') as w_file:
        w_file.write(username + ' ')
        w_file.write(secret_word + ' ')
        w_file.write(attempts + '\n')


def checkWordAndPrint(wrongLetters, rightLetters, secretWord):
    """

    params: wrongLetters is list of collected letters in list that aren't anywhere in word
        rightLetters is list of collected letters in list that are in secret_word
        secretWord is word that we are checking if contains characters that user inputed

    return: nothing, it just prints guessing word with _ in place of secret characters of 
        secret_word, and rightLetters in place in which guessed letters are in secret_word
    """
    
    for char in wrongLetters:
        print("Used letters:'{0}'".format(char), end='\n')

    guessing = '_' * len(secretWord)

    # Here we replace '_' with guessed letter 
    for current in range(len(secretWord)):
        if secretWord[current] in rightLetters:
            # This line saves everything left to current, on current it replaces '_' with
            # letter from user input, and shifts for one place to the right in case there
            # are multiple same letters in one word
            guessing = guessing[:current:] + secretWord[current] + guessing[current+1::]

    # Here we render once again, with updated guessed letters
    for char in guessing:
        print(char, end=' ')
    print()


def checkUserInput(guessed):
    """

    params: guessed - is paramtere that should get list of guessed letters
    return: guess - is user input that we check and send it further to process
    """
    while True:
        print("Guess a letter: ")
        guess = input().lower()
        if len(guess) != 1:
            print("Please enter just single letter.")
        elif guess in guessed:
            print("You have already guessed this letter, try other ones.")
        elif guess not in 'qwertyuiopasdfghjklzxcvbnm':
            print("Please enter a letter as an input for this game")
        else:
            return guess


def main():
    words = readFile(WORDLIST)

    # I thought separate function was not necessary for one liner
    # What it does it it get's us random word from list extracted from file
    secret_word = (random.choice(words)).lower()

    nick = input("Please enter your nick name: ")
    attempts = 5
    wrongLetters = ''
    rightLetters = ''
    gameLoop = True

    while gameLoop:
        checkWordAndPrint(wrongLetters, rightLetters, secret_word)
        guess = checkUserInput(wrongLetters + rightLetters)

        if guess in secret_word:
            rightLetters = rightLetters + guess

            guessedAll = True
            for i in range(len(secret_word)):
                if secret_word[i] not in rightLetters:
                    guessedAll = False
                    break

            if guessedAll:
                print("Congradulations! You guessed our secret word '{0}'".format(secret_word))
                attempts = str(attempts)
                writeFile(SCOREFILE, nick, secret_word, attempts)
                gameLoop = False
                
        else:
            wrongLetters = wrongLetters + guess
            attempts -= 1

            if attempts == 0:
                checkWordAndPrint(wrongLetters, rightLetters, secret_word)
                print("Game Over! Secret word was '{0}'".format(secret_word))
                attempts = str(attempts)
                writeFile(SCOREFILE, nick, secret_word, attempts)
                gameLoop = False


if __name__ == "__main__":
    main()
